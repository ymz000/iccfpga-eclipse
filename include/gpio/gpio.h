/*
-- IOTA Crypto Core
--
-- 2018 by Thomas Pototschnig <microengineer18@gmail.com>
-- discord: pmaxuw#8292
-- https://gitlab.com/iccfpga-rv
--
-- Permission is hereby granted, free of charge, to any person obtaining
-- a copy of this software and associated documentation files (the
-- "Software"), to deal in the Software without restriction, including
-- without limitation the rights to use, copy, modify, merge, publish,
-- distribute, sublicense, and/or sell copies of the Software, and to
-- permit persons to whom the Software is furnished to do so, subject to
-- the following conditions:
--
-- The above copyright notice and this permission notice shall be
-- included in all copies or substantial portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
-- EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
-- MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
-- NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
-- LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
-- OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
-- WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWAR
*/
#ifndef LED_H_
#define LED_H_

#define GPIO_LED_USER	(1<<19)
#define GPIO_LED_LOCK	(1<<20)

#define GPIO_MOSI0		(1<<0)
#define	GPIO_MISO1		(1<<1)
#define GPIO_SCK0		(1<<2)
#define GPIO_SS0		(1<<3)

#define GPIO_SS1		(1<<4)
#define GPIO_SCL0		(1<<5)
#define GPIO_SDA0		(1<<6)

#define GPIO_UART_RX	(1<<7)
#define GPIO_UART_TX	(1<<8)

namespace GPIO {
	int gpio_init();
	void setBit(int bit);
	void clrBit(int bit);
	//int readBit(int bit);
	void setAF(int bit, int state);
	void setDir(int bit, int state);
	uint32_t read();
}


#endif /* GPIO_H_ */
