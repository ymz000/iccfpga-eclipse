/*
-- IOTA Crypto Core
--
-- 2018 by Thomas Pototschnig <microengineer18@gmail.com>
-- discord: pmaxuw#8292
-- https://gitlab.com/iccfpga-rv
--
-- Permission is hereby granted, free of charge, to any person obtaining
-- a copy of this software and associated documentation files (the
-- "Software"), to deal in the Software without restriction, including
-- without limitation the rights to use, copy, modify, merge, publish,
-- distribute, sublicense, and/or sell copies of the Software, and to
-- permit persons to whom the Software is furnished to do so, subject to
-- the following conditions:
--
-- The above copyright notice and this permission notice shall be
-- included in all copies or substantial portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
-- EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
-- MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
-- NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
-- LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
-- OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
-- WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWAR
*/
#ifndef GPIO_H_
#define GPIO_H_

extern "C" void gpio_i2c_data_high();
extern "C" void gpio_i2c_data_low();
extern "C" void gpio_i2c_clock_high();
extern "C" void gpio_i2c_clock_low();
extern "C" int gpio_i2c_data_in();

namespace SecureGPIO {
	enum BitsOut {
		/*sda_o = (1<<0),*/ sda_t = (1<<1),
		/*scl_o = (1<<2),*/ scl_t = (1<<3),
		iprog = (1<<15)
	};

	enum BitsIn {
		sda_i = (1<<0), scl_1 = (1<<1)
	};

	int gpio_init();
	void setBit(BitsOut bit);
	void clrBit(BitsOut bit);
	int readBit(BitsIn bit);

	// trigger fpga reconfiguration
	void triggerIPROG(uint32_t wbstar);

};


#endif /* GPIO_H_ */
