
#ifndef BASE64_H_
#define BASE64_H_

#include <stdint.h>
#define size_t	uint32_t

namespace Base64 {
//	size_t getLengthDecode(size_t input_length);
//	size_t getLengthEncode(size_t input_length);
	void init();
	size_t base64_encode(const unsigned char *data, unsigned char* encoded_data, size_t input_length, size_t buffersize);
	size_t base64_decode(const char *data, unsigned char* decoded_data, size_t input_length, size_t buffersize);

}

#endif /* BASE64_H_ */
