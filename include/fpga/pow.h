/*
-- IOTA Crypto Core
--
-- 2018 by Thomas Pototschnig <microengineer18@gmail.com>
-- discord: pmaxuw#8292
-- https://gitlab.com/iccfpga-rv
--
-- Permission is hereby granted, free of charge, to any person obtaining
-- a copy of this software and associated documentation files (the
-- "Software"), to deal in the Software without restriction, including
-- without limitation the rights to use, copy, modify, merge, publish,
-- distribute, sublicense, and/or sell copies of the Software, and to
-- permit persons to whom the Software is furnished to do so, subject to
-- the following conditions:
-- 
-- The above copyright notice and this permission notice shall be
-- included in all copies or substantial portions of the Software.
-- 
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
-- EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
-- MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
-- NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
-- LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
-- OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
-- WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWAR
*/

#ifndef POW_H_
#define POW_H_

#define NONCE_TRINARY_SIZE	81

const char NONCE_INIT[27] = "PIDIVER9999999999999999999";

class Pow {
private:
	uint32_t m_parallel;
	int8_t m_nonceTrits[NONCE_TRINARY_SIZE];
	uint32_t m_log2;
	uint8_t m_tritsLUT[256];

protected:
	void powWriteFlags(char flag_start);
	uint32_t powReadParallelLevel();
	uint32_t powReadBinaryNonce();
	void powWriteMinWeightMagnitude(int bits);
	uint32_t powGetMask();
	uint32_t powGetFlags();
	void powCurlInit();

	uint8_t powTritToBits(char trit);
	uint8_t powBitsToTrit(uint8_t bits);

	void powCalcTrytesToTritsTable();

	void powCurlInitBlock();
	void powCurlSendBlock(const char* trytes, uint8_t docurl, bool isPoW);
	//void powInitMidState(const char* trytes);

public:
	void doPoW(const char* trytes, char* nonce, int mwm);
	int powCurlHash(const char* trytes, char* trytes_hash);

	void init();

};


#endif /* POW_H_ */
