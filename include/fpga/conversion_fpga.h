/*
-- IOTA Crypto Core
--
-- 2018 by Thomas Pototschnig <microengineer18@gmail.com>
-- discord: pmaxuw#8292
-- https://gitlab.com/iccfpga-rv
--
-- Permission is hereby granted, free of charge, to any person obtaining
-- a copy of this software and associated documentation files (the
-- "Software"), to deal in the Software without restriction, including
-- without limitation the rights to use, copy, modify, merge, publish,
-- distribute, sublicense, and/or sell copies of the Software, and to
-- permit persons to whom the Software is furnished to do so, subject to
-- the following conditions:
-- 
-- The above copyright notice and this permission notice shall be
-- included in all copies or substantial portions of the Software.
-- 
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
-- EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
-- MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
-- NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
-- LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
-- OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
-- WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWAR

Base: https://github.com/IOTA-Ledger/blue-app-iota

*/

#ifndef CONVERSION_FPGA_H_
#define CONVERSION_FPGA_H_

#include "iota/conversion.h"

class ConversionFPGA : public Conversion {
private:
	void fpga_write_address(uint32_t addr, uint32_t data);
	uint32_t fpga_read_address(uint32_t addr);
	uint32_t converterReadCtr();
	void converterUploadTrits(const trit_t* trits);
	void converterUploadBigint(const uint32_t* bigint);
	void converterDownloadTrits(trit_t* trits);
	void converterDownloadBigint(uint32_t* bigint);
	void converterUploadTrytes(const tryte_t* trytes);
	void converterDownloadTrytes(tryte_t* trytes);

	bool bigint_is_negative(const uint32_t *bigint);

public:
	void testClearData();

	virtual void trits_to_bigint(const trit_t *trits, uint32_t *bigint);
	virtual void bigint_to_trits(uint32_t *bigint, trit_t *trits);
	virtual void trits_to_bytes(const trit_t *trits, unsigned char *bytes);
	virtual void bytes_to_trits(const unsigned char *bytes, trit_t *trits);
//	virtual void bytes_to_trytes(const unsigned char *bytes, tryte_t *trytes);
	void trytes_to_bigint(const tryte_t *trytes, uint32_t *bigint);
	virtual void bigint_to_trytes_mem(uint32_t *bigint, tryte_t *trytes);

};



#endif /* CONVERSION_FPGA_H_ */
