/*
-- IOTA Crypto Core
--
-- 2018 by Thomas Pototschnig <microengineer18@gmail.com>
-- discord: pmaxuw#8292
-- https://gitlab.com/iccfpga-rv
--
-- Permission is hereby granted, free of charge, to any person obtaining
-- a copy of this software and associated documentation files (the
-- "Software"), to deal in the Software without restriction, including
-- without limitation the rights to use, copy, modify, merge, publish,
-- distribute, sublicense, and/or sell copies of the Software, and to
-- permit persons to whom the Software is furnished to do so, subject to
-- the following conditions:
-- 
-- The above copyright notice and this permission notice shall be
-- included in all copies or substantial portions of the Software.
-- 
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
-- EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
-- MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
-- NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
-- LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
-- OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
-- WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWAR
*/

#ifndef FPGA_H_
#define FPGA_H_

#include <stdint.h>

//#define STATE_LENGTH 729
//#define HASH_LENGTH 243
#define FLAG_RUNNING                    (1<<0)
#define FLAG_FOUND                      (1<<1)
#define FLAG_OVERFLOW                   (1<<2)
#define FLAG_CURL_FINISHED              (1<<3)


#define FLAG_START                      (1<<0)
#define FLAG_CURL_RESET                 (1<<1)
#define FLAG_CURL_WRITE                 (1<<2)
#define FLAG_CURL_DO_CURL               (1<<3)
#define FLAG_KECCAK384_START			(1<<4)
#define FLAG_KECCAK384_RESET			(1<<5)
#define FLAG_KECCAK384_RUNNING 			(1<<22)

#define FLAG_TROIKA_START				(1<<7)
#define FLAG_TROIKA_RESET				(1<<8)

// flag for accesssing the whole state (used in MAM)
#define FLAG_TROIKA_LOAD_RATE			(1<<9)
#define FLAG_TROIKA_LOAD_CAPACITY		(1<<10)
#define FLAG_MAM_RESET					(1<<11)

#define FLAG_SHA512_START				(1<<12)
#define FLAG_SHA512_RESET				(1<<13)

#define FLAG_TROIKA_RUNNING 			(1<<23)
#define FLAG_SHA512_RUNNING				(1<<21)


#define CMD_NOP                         0x00

#define CMD_WRITE_FLAGS                 0x01
#define CMD_WRITE_DATA                  0x80
#define CMD_READ_DATA					0x80
#define CMD_WRITE_MIN_WEIGHT_MAGNITUDE  0x02
#define CMD_WRITE_TROIKA_NUM_ROUNDS  	0x03
#define CMD_READ_FLAGS                  0x01
#define CMD_READ_NONCE                  0x02


namespace FPGA {
	void powInit();

	void pidiverWrite(uint32_t addr, uint32_t data);
	uint32_t pidiverRead(uint32_t addr);

	volatile uint32_t* pidiverGetDataWritePtr();
	volatile uint32_t* troikaReadDataPtr();
	volatile uint32_t* curlReadDataPtr();
	volatile uint32_t* keccak384ReadDataPtr();

	volatile uint32_t* troikaUncompressDataPtr();
	volatile uint32_t* converterGetTritsPtr();
	volatile uint32_t* converterGetBigintPtr();

	volatile uint32_t* getSHA512ReadPtr();

	void converterWrite(uint32_t addr, uint32_t data);
	uint32_t converterRead(uint32_t addr);

	void test();
};



#endif /* FPGA_H_ */
