/* sha3.h - an implementation of Secure Hash Algorithm 3 (Keccak).
 * based on the
 * The Keccak SHA-3 submission. Submission to NIST (Round 3), 2011
 * by Guido Bertoni, Joan Daemen, Michaël Peeters and Gilles Van Assche
 *
 * Copyright: 2013 Aleksey Kravchenko <rhash.admin@gmail.com>
 *
 * Permission is hereby granted,  free of charge,  to any person  obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction,  including without limitation
 * the rights to  use, copy, modify,  merge, publish, distribute, sublicense,
 * and/or sell copies  of  the Software,  and to permit  persons  to whom the
 * Software is furnished to do so.
 *
 * This program  is  distributed  in  the  hope  that it will be useful,  but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  Use this program  at  your own risk!
 */

#ifndef __SHA3_H__
#define __SHA3_H__

#include <stdint.h>
#include "options.h"

#define SHA3_384_USE_FPGA
#define SHA3_384_HASH_SIZE  48
#define SHA3_MAX_PERMUTATION_SIZE 25
#define SHA3_MAX_RATE_IN_QWORDS 24
#define SHA3_384_BLOCK_LENGTH   104
#define SHA3_384_DIGEST_LENGTH  sha3_384_hash_size



class Keccak384 {
private:
	struct ctxStruct {
		/* 1600 bits algorithm hashing state */
		uint64_t hash[SHA3_MAX_PERMUTATION_SIZE];
		/* 1536-bit buffer for leftovers */
		uint64_t message[SHA3_MAX_RATE_IN_QWORDS];
		/* count of bytes in the message[] buffer */
		unsigned rest;
		/* size of a message block processed at once */
	};
	ctxStruct ctx;

protected:
	void theta(uint64_t *A);
	void pi(uint64_t *A);
	void chi(uint64_t *A);
	void permutation(uint64_t *state);
	virtual void processBlock(uint64_t hash[25], const uint64_t *block);
	virtual void digest(unsigned char *result, uint32_t digest_length );

public:
	virtual void init();
	void update(const unsigned char* msg, uint32_t size);
	void final(unsigned char* result);
};



#endif /* __SHA3_H__ */
