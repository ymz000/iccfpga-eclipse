/*
 * This file is part of the µOS++ distribution.
 *   (https://github.com/micro-os-plus)
 * Copyright (c) 2017 Liviu Ionescu.
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom
 * the Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 */

//#include <micro-os-plus/board.h>
#include <micro-os-plus/startup/initialize-hooks.h>
#include "riscv.h"
#include <stdint.h>
// ----------------------------------------------------------------------------



// Volatile is normally not needed but it seems you have a special case
extern char __sup_stack;
//extern char __sup_stack_size;


// Called early, before copying .data and clearing .bss.
// Should initialize the clocks and possible other RAM areas.
void
os_startup_initialize_hardware_early (void)
{
	volatile uint32_t sup = (uint32_t) 0x84000800-4; //&__sup_stack;
	csr_write(mscratch, sup);
}

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wsign-conversion"

// Called before running the static constructors.
void
os_startup_initialize_hardware (void)
{
}

#pragma GCC diagnostic pop

// ----------------------------------------------------------------------------
