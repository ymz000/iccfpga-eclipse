/*
-- IOTA Crypto Core
--
-- 2018 by Thomas Pototschnig <microengineer18@gmail.com>
-- discord: pmaxuw#8292
-- https://gitlab.com/iccfpga-rv
--
-- Permission is hereby granted, free of charge, to any person obtaining
-- a copy of this software and associated documentation files (the
-- "Software"), to deal in the Software without restriction, including
-- without limitation the rights to use, copy, modify, merge, publish,
-- distribute, sublicense, and/or sell copies of the Software, and to
-- permit persons to whom the Software is furnished to do so, subject to
-- the following conditions:
-- 
-- The above copyright notice and this permission notice shall be
-- included in all copies or substantial portions of the Software.
-- 
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
-- EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
-- MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
-- NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
-- LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
-- OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
-- WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWAR
*/

#include <fpga/fpga.h>
#include <string.h>
#include "fpga/sha3.h"


// FPGA
void Keccak384FPGA::init() {
	Keccak384::init();
	FPGA::pidiverWrite(CMD_WRITE_FLAGS, FLAG_KECCAK384_RESET);
}

void Keccak384FPGA::processBlock(uint64_t hash[25], const uint64_t *block) {
	(void) hash;
	memcpy((uint32_t*) FPGA::pidiverGetDataWritePtr(), (uint32_t*) block, 26*sizeof(uint32_t));
	FPGA::pidiverWrite (CMD_WRITE_FLAGS, FLAG_KECCAK384_START);
	while (FPGA::pidiverRead(CMD_READ_FLAGS) & FLAG_KECCAK384_RUNNING) ;
}

void Keccak384FPGA::digest(unsigned char *result, uint32_t digest_length ) {
	memcpy(result, (void*) FPGA::keccak384ReadDataPtr()/*pidiverGetDataWritePtr()*/, digest_length);
}
