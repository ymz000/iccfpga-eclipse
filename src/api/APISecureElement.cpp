/*
 -- IOTA Crypto Core
 --
 -- 2018 by Thomas Pototschnig <microengineer18@gmail.com>
 -- discord: pmaxuw#8292
 -- https://gitlab.com/iccfpga-rv
 --
 -- Permission is hereby granted, free of charge, to any person obtaining
 -- a copy of this software and associated documentation files (the
 -- "Software"), to deal in the Software without restriction, including
 -- without limitation the rights to use, copy, modify, merge, publish,
 -- distribute, sublicense, and/or sell copies of the Software, and to
 -- permit persons to whom the Software is furnished to do so, subject to
 -- the following conditions:
 --
 -- The above copyright notice and this permission notice shall be
 -- included in all copies or substantial portions of the Software.
 --
 -- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 -- EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 -- MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 -- NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 -- LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 -- OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 -- WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWAR
 */

#include <unistd.h>
#include <ArduinoJson.h>

#include "api/API.h"

#include "iota/transaction.h"
#include "iota/transfers.h"
#include "iota/bundle.h"

#include "utils.h"

namespace API {
	int apiInitSecureElement() {
		if (
			!exists<const char*>(json, "key")
		) {
			return apiError(SBIError::MISSING_OR_INVALID);
		}

		const char* key = json["key"];
		if (!validateHex(key, 64))
			return apiError(SBIError::INVALID_KEY);

		SBIResponse* resp = SBI::initSecureElement(key);

		if (resp->isError())
			return apiError(resp->code);

		json.clear();
		return API_SUCCESS;
	}

	int apiSetRandomSeed() {
		if (
			!exists<uint32_t>(json, "slot")
		) {
			return apiError(SBIError::MISSING_OR_INVALID);
		}


		uint32_t slot = json["slot"];
		if (slot > 7) {
			return apiError(SBIError::INVALID_SLOT);
		}

		SBIResponse*resp = SBI::setRandomSeed(slot);
		if (resp->isError())
			return apiError(resp->code);

		json.clear();
		return API_SUCCESS;
	}

	bool registerAPISecureElement() {
		return
				registerAPICall("initSecureElement", &apiInitSecureElement) &&
				registerAPICall("generateRandomSeed", &apiSetRandomSeed);
	}

}


