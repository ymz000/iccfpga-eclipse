#ifndef XPARAMETERS_H   /* prevent circular inclusions */
#define XPARAMETERS_H   /* by using protection macros */

/* Definition for CPU ID */
#define XPAR_CPU_ID 0U

/* Definitions for peripheral CORTEXM1_AXI_0 */
#define XPAR_CORTEXM1_AXI_0_CPU_CLK_FREQ_HZ 0


/******************************************************************/

/* Canonical definitions for peripheral CORTEXM1_AXI_0 */
#define XPAR_CPU_CORTEXM1_0_CPU_CLK_FREQ_HZ 0


/******************************************************************/

#define STDIN_BASEADDRESS 0xf1070000
#define STDOUT_BASEADDRESS 0xf1070000

/******************************************************************/

/* Platform specific definitions */
#define PLATFORM_ARM
 
/* Definitions for sleep timer configuration */
#define XSLEEP_TIMER_IS_DEFAULT_TIMER
 
 
/******************************************************************/
/* Definitions for driver BRAM */
#define XPAR_XBRAM_NUM_INSTANCES 1U

/* Definitions for peripheral AXI_BRAM_CTRL_0 */
#define XPAR_AXI_BRAM_CTRL_0_DEVICE_ID 0U
#define XPAR_AXI_BRAM_CTRL_0_DATA_WIDTH 32U
#define XPAR_AXI_BRAM_CTRL_0_ECC 0U
#define XPAR_AXI_BRAM_CTRL_0_FAULT_INJECT 0U
#define XPAR_AXI_BRAM_CTRL_0_CE_FAILING_REGISTERS 0U
#define XPAR_AXI_BRAM_CTRL_0_UE_FAILING_REGISTERS 0U
#define XPAR_AXI_BRAM_CTRL_0_ECC_STATUS_REGISTERS 0U
#define XPAR_AXI_BRAM_CTRL_0_CE_COUNTER_WIDTH 0U
#define XPAR_AXI_BRAM_CTRL_0_ECC_ONOFF_REGISTER 0U
#define XPAR_AXI_BRAM_CTRL_0_ECC_ONOFF_RESET_VALUE 0U
#define XPAR_AXI_BRAM_CTRL_0_WRITE_ACCESS 0U
#define XPAR_AXI_BRAM_CTRL_0_S_AXI_BASEADDR 0xF4030000U
#define XPAR_AXI_BRAM_CTRL_0_S_AXI_HIGHADDR 0xF4031FFFU
#define XPAR_AXI_BRAM_CTRL_0_S_AXI_CTRL_BASEADDR 0xFFFFFFFFU 
#define XPAR_AXI_BRAM_CTRL_0_S_AXI_CTRL_HIGHADDR 0xFFFFFFFFU 


/******************************************************************/

/* Canonical definitions for peripheral AXI_BRAM_CTRL_0 */
#define XPAR_BRAM_0_DEVICE_ID XPAR_AXI_BRAM_CTRL_0_DEVICE_ID
#define XPAR_BRAM_0_DATA_WIDTH 32U
#define XPAR_BRAM_0_ECC 0U
#define XPAR_BRAM_0_FAULT_INJECT 0U
#define XPAR_BRAM_0_CE_FAILING_REGISTERS 0U
#define XPAR_BRAM_0_UE_FAILING_REGISTERS 0U
#define XPAR_BRAM_0_ECC_STATUS_REGISTERS 0U
#define XPAR_BRAM_0_CE_COUNTER_WIDTH 0U
#define XPAR_BRAM_0_ECC_ONOFF_REGISTER 0U
#define XPAR_BRAM_0_ECC_ONOFF_RESET_VALUE 0U
#define XPAR_BRAM_0_WRITE_ACCESS 0U
#define XPAR_BRAM_0_BASEADDR 0xF1080000U
#define XPAR_BRAM_0_HIGHADDR 0xF1081FFFU


/******************************************************************/


/* Definitions for peripheral AXI2LB_V1_0_0 */
#define XPAR_AXI2LB_V1_0_0_BASEADDR 0xf4050000
#define XPAR_AXI2LB_V1_0_0_HIGHADDR 0xf405FFFF


/* Definitions for peripheral AXI2LB_V1_0_1 */
#define XPAR_AXI2LB_V1_0_1_BASEADDR 0xf4040000
#define XPAR_AXI2LB_V1_0_1_HIGHADDR 0xf404FFFF

#define XPAR_TIMER_0_BASEADDR		0xf4010000


/******************************************************************/

/* Definitions for driver GPIO */
#define XPAR_XGPIO_NUM_INSTANCES 2

/* Definitions for peripheral AXI_GPIO_0 */
#define XPAR_AXI_GPIO_0_BASEADDR 0xf1030000
#define XPAR_AXI_GPIO_0_HIGHADDR 0xf103ffff
#define XPAR_AXI_GPIO_0_DEVICE_ID 0
#define XPAR_AXI_GPIO_0_INTERRUPT_PRESENT 0
#define XPAR_AXI_GPIO_0_IS_DUAL 1


/* Definitions for peripheral AXI_GPIO_1 */
#define XPAR_AXI_GPIO_1_BASEADDR 0xf4000000
#define XPAR_AXI_GPIO_1_HIGHADDR 0xf400ffff
#define XPAR_AXI_GPIO_1_DEVICE_ID 1
#define XPAR_AXI_GPIO_1_INTERRUPT_PRESENT 0
#define XPAR_AXI_GPIO_1_IS_DUAL 1


/******************************************************************/

/* Canonical definitions for peripheral AXI_GPIO_0 */
#define XPAR_GPIO_0_BASEADDR 0xf1030000 //0x40000000
#define XPAR_GPIO_0_HIGHADDR 0xf103ffff //0x4000FFFF
#define XPAR_GPIO_0_DEVICE_ID XPAR_AXI_GPIO_0_DEVICE_ID
#define XPAR_GPIO_0_INTERRUPT_PRESENT 0
#define XPAR_GPIO_0_IS_DUAL 1

/* Canonical definitions for peripheral AXI_GPIO_1 */
#define XPAR_GPIO_1_BASEADDR 0xf4000000 //0x40010000
#define XPAR_GPIO_1_HIGHADDR 0xf400ffff //0x4001FFFF
#define XPAR_GPIO_1_DEVICE_ID XPAR_AXI_GPIO_1_DEVICE_ID
#define XPAR_GPIO_1_INTERRUPT_PRESENT 0
#define XPAR_GPIO_1_IS_DUAL 1


/******************************************************************/

/* Definitions for driver SPI */
#define XPAR_XSPI_NUM_INSTANCES 2U //3U

/* Definitions for peripheral AXI_QUAD_SPI_0 */
#define XPAR_AXI_QUAD_SPI_0_DEVICE_ID 0U
#define XPAR_AXI_QUAD_SPI_0_BASEADDR 0xf1040000U
#define XPAR_AXI_QUAD_SPI_0_HIGHADDR 0xf104FFFFU
#define XPAR_AXI_QUAD_SPI_0_FIFO_DEPTH 16U
#define XPAR_AXI_QUAD_SPI_0_FIFO_EXIST 1U
#define XPAR_AXI_QUAD_SPI_0_SPI_SLAVE_ONLY 0U
#define XPAR_AXI_QUAD_SPI_0_NUM_SS_BITS 1U
#define XPAR_AXI_QUAD_SPI_0_NUM_TRANSFER_BITS 8U
#define XPAR_AXI_QUAD_SPI_0_SPI_MODE 0U
#define XPAR_AXI_QUAD_SPI_0_TYPE_OF_AXI4_INTERFACE 0U
#define XPAR_AXI_QUAD_SPI_0_AXI4_BASEADDR 0U
#define XPAR_AXI_QUAD_SPI_0_AXI4_HIGHADDR 0U
#define XPAR_AXI_QUAD_SPI_0_XIP_MODE 0U

/* Canonical definitions for peripheral AXI_QUAD_SPI_0 */
#define XPAR_SPI_0_DEVICE_ID 0U
#define XPAR_SPI_0_BASEADDR 0xf1040000U
#define XPAR_SPI_0_HIGHADDR 0xf104FFFFU
#define XPAR_SPI_0_FIFO_DEPTH 16U
#define XPAR_SPI_0_FIFO_EXIST 1U
#define XPAR_SPI_0_SPI_SLAVE_ONLY 0U
#define XPAR_SPI_0_NUM_SS_BITS 1U
#define XPAR_SPI_0_NUM_TRANSFER_BITS 8U
#define XPAR_SPI_0_SPI_MODE 0U
#define XPAR_SPI_0_TYPE_OF_AXI4_INTERFACE 0U
#define XPAR_SPI_0_AXI4_BASEADDR 0U
#define XPAR_SPI_0_AXI4_HIGHADDR 0U
#define XPAR_SPI_0_XIP_MODE 0U
#define XPAR_SPI_0_USE_STARTUP 0U



/* Definitions for peripheral AXI_QUAD_SPI_1 */
#define XPAR_AXI_QUAD_SPI_1_DEVICE_ID 1U
#define XPAR_AXI_QUAD_SPI_1_BASEADDR 0xf4020000U
#define XPAR_AXI_QUAD_SPI_1_HIGHADDR 0xf402FFFFU
#define XPAR_AXI_QUAD_SPI_1_FIFO_DEPTH 16U
#define XPAR_AXI_QUAD_SPI_1_FIFO_EXIST 1U
#define XPAR_AXI_QUAD_SPI_1_SPI_SLAVE_ONLY 0U
#define XPAR_AXI_QUAD_SPI_1_NUM_SS_BITS 1U
#define XPAR_AXI_QUAD_SPI_1_NUM_TRANSFER_BITS 8U
#define XPAR_AXI_QUAD_SPI_1_SPI_MODE 0U
#define XPAR_AXI_QUAD_SPI_1_TYPE_OF_AXI4_INTERFACE 0U
#define XPAR_AXI_QUAD_SPI_1_AXI4_BASEADDR 0U
#define XPAR_AXI_QUAD_SPI_1_AXI4_HIGHADDR 0U
#define XPAR_AXI_QUAD_SPI_1_XIP_MODE 0U

/* Canonical definitions for peripheral AXI_QUAD_SPI_1 */
#define XPAR_SPI_1_DEVICE_ID 1U
#define XPAR_SPI_1_BASEADDR 0xf4020000U
#define XPAR_SPI_1_HIGHADDR 0xf402FFFFU
#define XPAR_SPI_1_FIFO_DEPTH 16U
#define XPAR_SPI_1_FIFO_EXIST 1U
#define XPAR_SPI_1_SPI_SLAVE_ONLY 0U
#define XPAR_SPI_1_NUM_SS_BITS 1U
#define XPAR_SPI_1_NUM_TRANSFER_BITS 8U
#define XPAR_SPI_1_SPI_MODE 0U
#define XPAR_SPI_1_TYPE_OF_AXI4_INTERFACE 0U
#define XPAR_SPI_1_AXI4_BASEADDR 0U
#define XPAR_SPI_1_AXI4_HIGHADDR 0U
#define XPAR_SPI_1_XIP_MODE 0U
#define XPAR_SPI_1_USE_STARTUP 0U

#if 0
/* Definitions for peripheral AXI_QUAD_SPI_2 */
#define XPAR_AXI_QUAD_SPI_2_DEVICE_ID 2U
#define XPAR_AXI_QUAD_SPI_2_BASEADDR 0xf0070000U
#define XPAR_AXI_QUAD_SPI_2_HIGHADDR 0xf007FFFFU
#define XPAR_AXI_QUAD_SPI_2_FIFO_DEPTH 16U
#define XPAR_AXI_QUAD_SPI_2_FIFO_EXIST 1U
#define XPAR_AXI_QUAD_SPI_2_SPI_SLAVE_ONLY 0U
#define XPAR_AXI_QUAD_SPI_2_NUM_SS_BITS 1U
#define XPAR_AXI_QUAD_SPI_2_NUM_TRANSFER_BITS 8U
#define XPAR_AXI_QUAD_SPI_2_SPI_MODE 2U
#define XPAR_AXI_QUAD_SPI_2_TYPE_OF_AXI4_INTERFACE 0U
#define XPAR_AXI_QUAD_SPI_2_AXI4_BASEADDR 0U
#define XPAR_AXI_QUAD_SPI_2_AXI4_HIGHADDR 0U
#define XPAR_AXI_QUAD_SPI_2_XIP_MODE 0U

/* Canonical definitions for peripheral AXI_QUAD_SPI_2 */
#define XPAR_SPI_2_DEVICE_ID 2U
#define XPAR_SPI_2_BASEADDR 0xf0070000U
#define XPAR_SPI_2_HIGHADDR 0xf007FFFFU
#define XPAR_SPI_2_FIFO_DEPTH 16U
#define XPAR_SPI_2_FIFO_EXIST 1U
#define XPAR_SPI_2_SPI_SLAVE_ONLY 0U
#define XPAR_SPI_2_NUM_SS_BITS 1U
#define XPAR_SPI_2_NUM_TRANSFER_BITS 8U
#define XPAR_SPI_2_SPI_MODE 2U
#define XPAR_SPI_2_TYPE_OF_AXI4_INTERFACE 0U
#define XPAR_SPI_2_AXI4_BASEADDR 0U
#define XPAR_SPI_2_AXI4_HIGHADDR 0U
#define XPAR_SPI_2_XIP_MODE 0U
#define XPAR_SPI_2_USE_STARTUP 1U
#endif
#if 0
/* Definitions for driver QSPIPS */
#define XPAR_XQSPIPS_NUM_INSTANCES 1

/* Definitions for peripheral PS7_QSPI_0 */
#define XPAR_PS7_QSPI_0_DEVICE_ID 0
#define XPAR_PS7_QSPI_0_BASEADDR 0xF1060000
#define XPAR_PS7_QSPI_0_HIGHADDR 0xF106FFFF
#define XPAR_PS7_QSPI_0_QSPI_CLK_FREQ_HZ 200000000
#define XPAR_PS7_QSPI_0_QSPI_MODE 0

/* Canonical definitions for peripheral PS7_QSPI_0 */
#define XPAR_XQSPIPS_0_DEVICE_ID            XPAR_PS7_QSPI_0_DEVICE_ID
#define XPAR_XQSPIPS_0_BASEADDR             XPAR_PS7_QSPI_0_BASEADDR
#define XPAR_XQSPIPS_0_HIGHADDR             XPAR_PS7_QSPI_0_HIGHADDR
#define XPAR_XQSPIPS_0_QSPI_CLK_FREQ_HZ     XPAR_PS7_QSPI_0_QSPI_CLK_FREQ_HZ   // todo actually 50MHz ...
#define XPAR_XQSPIPS_0_QSPI_MODE            XPAR_PS7_QSPI_0_QSPI_MODE

// needed in xqspips_hw.h
#define XPAR_XSLCR_0_BASEADDR 				XPAR_PS7_QSPI_0_BASEADDR
#endif
/******************************************************************/

/* Definitions for driver UARTLITE */
#define XPAR_XUARTLITE_NUM_INSTANCES 1


/* Definitions for peripheral AXI_UARTLITE_1 */
#define XPAR_AXI_UARTLITE_0_BASEADDR 0xf1070000
#define XPAR_AXI_UARTLITE_0_HIGHADDR 0xf107FFFF
#define XPAR_AXI_UARTLITE_0_DEVICE_ID 0
#define XPAR_AXI_UARTLITE_0_BAUDRATE 115200
#define XPAR_AXI_UARTLITE_0_USE_PARITY 0
#define XPAR_AXI_UARTLITE_0_ODD_PARITY 0
#define XPAR_AXI_UARTLITE_0_DATA_BITS 8

/******************************************************************/


/* Canonical definitions for peripheral AXI_UARTLITE_0 */
#define XPAR_UARTLITE_0_DEVICE_ID XPAR_AXI_UARTLITE_0_DEVICE_ID
#define XPAR_UARTLITE_0_BASEADDR 0xf1070000
#define XPAR_UARTLITE_0_HIGHADDR 0xf107FFFF
#define XPAR_UARTLITE_0_BAUDRATE 115200
#define XPAR_UARTLITE_0_USE_PARITY 0
#define XPAR_UARTLITE_0_ODD_PARITY 0
#define XPAR_UARTLITE_0_DATA_BITS 8

/******************************************************************/

#endif  /* end of protection macro */
