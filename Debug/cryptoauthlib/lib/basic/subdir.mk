################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../cryptoauthlib/lib/basic/atca_basic.c \
../cryptoauthlib/lib/basic/atca_basic_aes.c \
../cryptoauthlib/lib/basic/atca_basic_checkmac.c \
../cryptoauthlib/lib/basic/atca_basic_counter.c \
../cryptoauthlib/lib/basic/atca_basic_derivekey.c \
../cryptoauthlib/lib/basic/atca_basic_ecdh.c \
../cryptoauthlib/lib/basic/atca_basic_gendig.c \
../cryptoauthlib/lib/basic/atca_basic_genkey.c \
../cryptoauthlib/lib/basic/atca_basic_hmac.c \
../cryptoauthlib/lib/basic/atca_basic_info.c \
../cryptoauthlib/lib/basic/atca_basic_kdf.c \
../cryptoauthlib/lib/basic/atca_basic_lock.c \
../cryptoauthlib/lib/basic/atca_basic_mac.c \
../cryptoauthlib/lib/basic/atca_basic_nonce.c \
../cryptoauthlib/lib/basic/atca_basic_privwrite.c \
../cryptoauthlib/lib/basic/atca_basic_random.c \
../cryptoauthlib/lib/basic/atca_basic_read.c \
../cryptoauthlib/lib/basic/atca_basic_secureboot.c \
../cryptoauthlib/lib/basic/atca_basic_selftest.c \
../cryptoauthlib/lib/basic/atca_basic_sha.c \
../cryptoauthlib/lib/basic/atca_basic_sign.c \
../cryptoauthlib/lib/basic/atca_basic_updateextra.c \
../cryptoauthlib/lib/basic/atca_basic_verify.c \
../cryptoauthlib/lib/basic/atca_basic_write.c \
../cryptoauthlib/lib/basic/atca_helpers.c 

OBJS += \
./cryptoauthlib/lib/basic/atca_basic.o \
./cryptoauthlib/lib/basic/atca_basic_aes.o \
./cryptoauthlib/lib/basic/atca_basic_checkmac.o \
./cryptoauthlib/lib/basic/atca_basic_counter.o \
./cryptoauthlib/lib/basic/atca_basic_derivekey.o \
./cryptoauthlib/lib/basic/atca_basic_ecdh.o \
./cryptoauthlib/lib/basic/atca_basic_gendig.o \
./cryptoauthlib/lib/basic/atca_basic_genkey.o \
./cryptoauthlib/lib/basic/atca_basic_hmac.o \
./cryptoauthlib/lib/basic/atca_basic_info.o \
./cryptoauthlib/lib/basic/atca_basic_kdf.o \
./cryptoauthlib/lib/basic/atca_basic_lock.o \
./cryptoauthlib/lib/basic/atca_basic_mac.o \
./cryptoauthlib/lib/basic/atca_basic_nonce.o \
./cryptoauthlib/lib/basic/atca_basic_privwrite.o \
./cryptoauthlib/lib/basic/atca_basic_random.o \
./cryptoauthlib/lib/basic/atca_basic_read.o \
./cryptoauthlib/lib/basic/atca_basic_secureboot.o \
./cryptoauthlib/lib/basic/atca_basic_selftest.o \
./cryptoauthlib/lib/basic/atca_basic_sha.o \
./cryptoauthlib/lib/basic/atca_basic_sign.o \
./cryptoauthlib/lib/basic/atca_basic_updateextra.o \
./cryptoauthlib/lib/basic/atca_basic_verify.o \
./cryptoauthlib/lib/basic/atca_basic_write.o \
./cryptoauthlib/lib/basic/atca_helpers.o 

C_DEPS += \
./cryptoauthlib/lib/basic/atca_basic.d \
./cryptoauthlib/lib/basic/atca_basic_aes.d \
./cryptoauthlib/lib/basic/atca_basic_checkmac.d \
./cryptoauthlib/lib/basic/atca_basic_counter.d \
./cryptoauthlib/lib/basic/atca_basic_derivekey.d \
./cryptoauthlib/lib/basic/atca_basic_ecdh.d \
./cryptoauthlib/lib/basic/atca_basic_gendig.d \
./cryptoauthlib/lib/basic/atca_basic_genkey.d \
./cryptoauthlib/lib/basic/atca_basic_hmac.d \
./cryptoauthlib/lib/basic/atca_basic_info.d \
./cryptoauthlib/lib/basic/atca_basic_kdf.d \
./cryptoauthlib/lib/basic/atca_basic_lock.d \
./cryptoauthlib/lib/basic/atca_basic_mac.d \
./cryptoauthlib/lib/basic/atca_basic_nonce.d \
./cryptoauthlib/lib/basic/atca_basic_privwrite.d \
./cryptoauthlib/lib/basic/atca_basic_random.d \
./cryptoauthlib/lib/basic/atca_basic_read.d \
./cryptoauthlib/lib/basic/atca_basic_secureboot.d \
./cryptoauthlib/lib/basic/atca_basic_selftest.d \
./cryptoauthlib/lib/basic/atca_basic_sha.d \
./cryptoauthlib/lib/basic/atca_basic_sign.d \
./cryptoauthlib/lib/basic/atca_basic_updateextra.d \
./cryptoauthlib/lib/basic/atca_basic_verify.d \
./cryptoauthlib/lib/basic/atca_basic_write.d \
./cryptoauthlib/lib/basic/atca_helpers.d 


# Each subdirectory must supply rules for building sources it contributes
cryptoauthlib/lib/basic/%.o: ../cryptoauthlib/lib/basic/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: GNU RISC-V Cross C Compiler'
	riscv32-unknown-elf-gcc -march=rv32imac -mabi=ilp32 -mcmodel=medany -msmall-data-limit=8 -mno-save-restore -Og -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -fno-move-loop-invariants -Wall -Wextra  -g3 -DDEBUG -DOS_USE_CPP_INTERRUPTS -DSIFIVE_FE310 -DSIFIVE_HIFIVE1_BOARD -DATCA_HAL_I2C -DATCA_NO_HEAP -DTRACE -DOS_USE_TRACE_SEMIHOSTING_DEBUG -DARDUINOJSON_EMBEDDED_MODE=1 -DARDUINOJSON_USE_LONG_LONG=1 -I"../include" -I"../xpacks/micro-os-plus-diag-trace/include" -I"../xpacks/micro-os-plus-semihosting/include" -I"../xpacks/sifive-devices/include" -I"../xpacks/sifive-hifive1-board/include" -I"../xpacks/micro-os-plus-c-libs/include" -I"../xpacks/micro-os-plus-riscv-arch/include" -I"../xpacks/micro-os-plus-startup/include" -I"/home/thomas/programme/ext/newrepo/iccfpga_wallet/iccfpga-eclipse/bsp_iccfpga/RISC_V/include" -I"/home/thomas/programme/ext/newrepo/iccfpga_wallet/iccfpga-eclipse" -I"/home/thomas/programme/ext/newrepo/iccfpga_wallet/iccfpga-eclipse/cryptoauthlib/lib" -I"/home/thomas/programme/ext/newrepo/iccfpga_wallet/iccfpga-eclipse/submodule/ArduinoJson/src" -I"/home/thomas/programme/ext/newrepo/iccfpga_wallet/iccfpga-eclipse/submodule/iccfpga-iota-lib/src" -I"/home/thomas/programme/ext/newrepo/iccfpga_wallet/iccfpga-eclipse/src/sha512" -I"/home/thomas/programme/ext/newrepo/iccfpga_wallet/iccfpga-eclipse/submodule/trezor-crypto" -std=gnu11 -Os -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


