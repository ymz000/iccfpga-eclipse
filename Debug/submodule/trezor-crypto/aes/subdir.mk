################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../submodule/trezor-crypto/aes/aes_modes.c \
../submodule/trezor-crypto/aes/aescrypt.c \
../submodule/trezor-crypto/aes/aeskey.c \
../submodule/trezor-crypto/aes/aestab.c 

OBJS += \
./submodule/trezor-crypto/aes/aes_modes.o \
./submodule/trezor-crypto/aes/aescrypt.o \
./submodule/trezor-crypto/aes/aeskey.o \
./submodule/trezor-crypto/aes/aestab.o 

C_DEPS += \
./submodule/trezor-crypto/aes/aes_modes.d \
./submodule/trezor-crypto/aes/aescrypt.d \
./submodule/trezor-crypto/aes/aeskey.d \
./submodule/trezor-crypto/aes/aestab.d 


# Each subdirectory must supply rules for building sources it contributes
submodule/trezor-crypto/aes/%.o: ../submodule/trezor-crypto/aes/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: GNU RISC-V Cross C Compiler'
	riscv32-unknown-elf-gcc -march=rv32ima -mabi=ilp32 -mcmodel=medany -msmall-data-limit=8 -mno-save-restore -O1 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -fno-move-loop-invariants -Wall -Wextra  -g3 -DDEBUG -DOS_USE_CPP_INTERRUPTS -DSIFIVE_FE310 -DSIFIVE_HIFIVE1_BOARD -DATCA_HAL_I2C -DATCA_NO_HEAP -DTRACE -DOS_USE_TRACE_SEMIHOSTING_DEBUG -I"../include" -I"../xpacks/micro-os-plus-diag-trace/include" -I"../xpacks/micro-os-plus-semihosting/include" -I"../xpacks/sifive-devices/include" -I"../xpacks/sifive-hifive1-board/include" -I"../xpacks/micro-os-plus-c-libs/include" -I"../xpacks/micro-os-plus-riscv-arch/include" -I"../xpacks/micro-os-plus-startup/include" -I"/home/ne23kaj2/programme/ext/newrepo/iccfpga_wallet/iccfpga-eclipse/bsp_iccfpga/RISC_V/include" -I"/home/ne23kaj2/programme/ext/newrepo/iccfpga_wallet/iccfpga-eclipse" -I"/home/ne23kaj2/programme/ext/newrepo/iccfpga_wallet/iccfpga-eclipse/cryptoauthlib/lib" -I"/home/ne23kaj2/programme/ext/newrepo/iccfpga_wallet/iccfpga-eclipse/submodule/ArduinoJson/src" -I"/home/ne23kaj2/programme/ext/newrepo/iccfpga_wallet/iccfpga-eclipse/submodule/iccfpga-iota-lib/src" -I"/home/ne23kaj2/programme/ext/newrepo/iccfpga_wallet/iccfpga-eclipse/src/sha512" -I"/home/ne23kaj2/programme/ext/newrepo/iccfpga_wallet/iccfpga-eclipse/submodule/trezor-crypto" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


