################################################################################
# Automatically-generated file. Do not edit!
################################################################################

ELF_SRCS := 
C_UPPER_SRCS := 
CXX_SRCS := 
C++_SRCS := 
OBJ_SRCS := 
CC_SRCS := 
ASM_SRCS := 
C_SRCS := 
CPP_SRCS := 
S_UPPER_SRCS := 
O_SRCS := 
CC_DEPS := 
C++_DEPS := 
OBJS := 
C_UPPER_DEPS := 
CXX_DEPS := 
SECONDARY_FLASH := 
SECONDARY_SIZE := 
ASM_DEPS := 
S_UPPER_DEPS := 
C_DEPS := 
CPP_DEPS := 

# Every subdirectory with source files must be described here
SUBDIRS := \
bsp_iccfpga/RISC_V/libsrc/bram_v4_2/src \
bsp_iccfpga/RISC_V/libsrc/gpio_v4_3/src \
bsp_iccfpga/RISC_V/libsrc/spi_v4_4/src \
bsp_iccfpga/RISC_V/libsrc/standalone_v6_7/src \
bsp_iccfpga/RISC_V/libsrc/uartlite_v3_2/src \
cryptoauthlib/lib \
cryptoauthlib/lib/atcacert \
cryptoauthlib/lib/basic \
cryptoauthlib/lib/crypto \
cryptoauthlib/lib/crypto/hashes \
cryptoauthlib/lib/hal \
cryptoauthlib/lib/host \
cryptoauthlib/lib/jwt \
src \
src/api \
src/fpga \
src/gpio \
src/keccak \
src/qspi \
src/secure \
src/sha512 \
src/ssd1331 \
src/troika \
submodule/iccfpga-iota-lib/src \
submodule/iccfpga-iota-lib/src/iota \
submodule/trezor-crypto \
xpacks/micro-os-plus-c-libs/src \
xpacks/micro-os-plus-c-libs/src/stdlib \
xpacks/micro-os-plus-cpp-libs/src \
xpacks/micro-os-plus-diag-trace/src \
xpacks/micro-os-plus-riscv-arch/src \
xpacks/micro-os-plus-semihosting/src \
xpacks/micro-os-plus-startup/src \

