################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../src/secure/delays.cpp \
../src/secure/pmp.cpp \
../src/secure/sbi.cpp \
../src/secure/secure.cpp 

OBJS += \
./src/secure/delays.o \
./src/secure/pmp.o \
./src/secure/sbi.o \
./src/secure/secure.o 

CPP_DEPS += \
./src/secure/delays.d \
./src/secure/pmp.d \
./src/secure/sbi.d \
./src/secure/secure.d 


# Each subdirectory must supply rules for building sources it contributes
src/secure/%.o: ../src/secure/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GNU RISC-V Cross C++ Compiler'
	riscv32-unknown-elf-g++ -march=rv32imac -mabi=ilp32 -mcmodel=medany -msmall-data-limit=8 -mno-save-restore -Os -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -Wall -Wextra  -g -DOS_USE_CPP_INTERRUPTS -DNDEBUG -DSIFIVE_FE310 -DSIFIVE_HIFIVE1_BOARD -DATCA_HAL_I2C -DATCA_NO_HEAP -DARDUINOJSON_EMBEDDED_MODE=1 -DENABLE_FPGA_TESTS -DARDUINOJSON_USE_LONG_LONG=1 -I"../include" -I"../xpacks/micro-os-plus-diag-trace/include" -I"../xpacks/micro-os-plus-semihosting/include" -I"../xpacks/sifive-devices/include" -I"../xpacks/sifive-hifive1-board/include" -I"../xpacks/micro-os-plus-c-libs/include" -I"../xpacks/micro-os-plus-riscv-arch/include" -I"../xpacks/micro-os-plus-startup/include" -I"/home/thomas/programme/ext/newrepo/iccfpga_wallet/iccfpga-eclipse/bsp_iccfpga/RISC_V/include" -I"/home/thomas/programme/ext/newrepo/iccfpga_wallet/iccfpga-eclipse" -I"/home/thomas/programme/ext/newrepo/iccfpga_wallet/iccfpga-eclipse/cryptoauthlib/lib" -I"/home/thomas/programme/ext/newrepo/iccfpga_wallet/iccfpga-eclipse/submodule/ArduinoJson/src" -I"/home/thomas/programme/ext/newrepo/iccfpga_wallet/iccfpga-eclipse/submodule/iccfpga-iota-lib/src" -I"/home/thomas/programme/ext/newrepo/iccfpga_wallet/iccfpga-eclipse/src/sha512" -I"/home/thomas/programme/ext/newrepo/iccfpga_wallet/iccfpga-eclipse/submodule/trezor-crypto" -std=gnu++14 -fabi-version=0 -fno-exceptions -fno-rtti -fno-use-cxa-atexit -fno-threadsafe-statics -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


