################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../cryptoauthlib/lib/atcacert/atcacert_client.c \
../cryptoauthlib/lib/atcacert/atcacert_date.c \
../cryptoauthlib/lib/atcacert/atcacert_def.c \
../cryptoauthlib/lib/atcacert/atcacert_der.c \
../cryptoauthlib/lib/atcacert/atcacert_host_hw.c \
../cryptoauthlib/lib/atcacert/atcacert_host_sw.c \
../cryptoauthlib/lib/atcacert/atcacert_pem.c 

OBJS += \
./cryptoauthlib/lib/atcacert/atcacert_client.o \
./cryptoauthlib/lib/atcacert/atcacert_date.o \
./cryptoauthlib/lib/atcacert/atcacert_def.o \
./cryptoauthlib/lib/atcacert/atcacert_der.o \
./cryptoauthlib/lib/atcacert/atcacert_host_hw.o \
./cryptoauthlib/lib/atcacert/atcacert_host_sw.o \
./cryptoauthlib/lib/atcacert/atcacert_pem.o 

C_DEPS += \
./cryptoauthlib/lib/atcacert/atcacert_client.d \
./cryptoauthlib/lib/atcacert/atcacert_date.d \
./cryptoauthlib/lib/atcacert/atcacert_def.d \
./cryptoauthlib/lib/atcacert/atcacert_der.d \
./cryptoauthlib/lib/atcacert/atcacert_host_hw.d \
./cryptoauthlib/lib/atcacert/atcacert_host_sw.d \
./cryptoauthlib/lib/atcacert/atcacert_pem.d 


# Each subdirectory must supply rules for building sources it contributes
cryptoauthlib/lib/atcacert/%.o: ../cryptoauthlib/lib/atcacert/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: GNU RISC-V Cross C Compiler'
	riscv32-unknown-elf-gcc -march=rv32imac -mabi=ilp32 -mcmodel=medany -msmall-data-limit=8 -mno-save-restore -Os -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -Wall -Wextra  -g -DOS_USE_CPP_INTERRUPTS -DNDEBUG -DSIFIVE_FE310 -DSIFIVE_HIFIVE1_BOARD -DATCA_HAL_I2C -DATCA_NO_HEAP -DARDUINOJSON_EMBEDDED_MODE=1 -DENABLE_FPGA_TESTS -DARDUINOJSON_USE_LONG_LONG=1 -I"../include" -I"../xpacks/micro-os-plus-diag-trace/include" -I"../xpacks/micro-os-plus-semihosting/include" -I"../xpacks/sifive-devices/include" -I"../xpacks/sifive-hifive1-board/include" -I"../xpacks/micro-os-plus-c-libs/include" -I"../xpacks/micro-os-plus-riscv-arch/include" -I"../xpacks/micro-os-plus-startup/include" -I"/home/thomas/programme/ext/newrepo/iccfpga_wallet/iccfpga-eclipse/bsp_iccfpga/RISC_V/include" -I"/home/thomas/programme/ext/newrepo/iccfpga_wallet/iccfpga-eclipse" -I"/home/thomas/programme/ext/newrepo/iccfpga_wallet/iccfpga-eclipse/cryptoauthlib/lib" -I"/home/thomas/programme/ext/newrepo/iccfpga_wallet/iccfpga-eclipse/submodule/ArduinoJson/src" -I"/home/thomas/programme/ext/newrepo/iccfpga_wallet/iccfpga-eclipse/submodule/iccfpga-iota-lib/src" -I"/home/thomas/programme/ext/newrepo/iccfpga_wallet/iccfpga-eclipse/src/sha512" -I"/home/thomas/programme/ext/newrepo/iccfpga_wallet/iccfpga-eclipse/submodule/trezor-crypto" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


