################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../submodule/trezor-crypto/monero/base58.c \
../submodule/trezor-crypto/monero/range_proof.c \
../submodule/trezor-crypto/monero/serialize.c \
../submodule/trezor-crypto/monero/xmr.c 

OBJS += \
./submodule/trezor-crypto/monero/base58.o \
./submodule/trezor-crypto/monero/range_proof.o \
./submodule/trezor-crypto/monero/serialize.o \
./submodule/trezor-crypto/monero/xmr.o 

C_DEPS += \
./submodule/trezor-crypto/monero/base58.d \
./submodule/trezor-crypto/monero/range_proof.d \
./submodule/trezor-crypto/monero/serialize.d \
./submodule/trezor-crypto/monero/xmr.d 


# Each subdirectory must supply rules for building sources it contributes
submodule/trezor-crypto/monero/%.o: ../submodule/trezor-crypto/monero/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: GNU RISC-V Cross C Compiler'
	riscv32-unknown-elf-gcc -march=rv32ima -mabi=ilp32 -mcmodel=medany -msmall-data-limit=8 -mno-save-restore -Os -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -Wall -Wextra  -g -DOS_USE_CPP_INTERRUPTS -DNDEBUG -DSIFIVE_FE310 -DSIFIVE_HIFIVE1_BOARD -DATCA_HAL_I2C -DATCA_NO_HEAP -I"../include" -I"../xpacks/micro-os-plus-diag-trace/include" -I"../xpacks/micro-os-plus-semihosting/include" -I"../xpacks/sifive-devices/include" -I"../xpacks/sifive-hifive1-board/include" -I"../xpacks/micro-os-plus-c-libs/include" -I"../xpacks/micro-os-plus-riscv-arch/include" -I"../xpacks/micro-os-plus-startup/include" -I"/home/ne23kaj2/programme/ext/newrepo/iccfpga_wallet/iccfpga-eclipse/bsp_iccfpga/RISC_V/include" -I"/home/ne23kaj2/programme/ext/newrepo/iccfpga_wallet/iccfpga-eclipse" -I"/home/ne23kaj2/programme/ext/newrepo/iccfpga_wallet/iccfpga-eclipse/cryptoauthlib/lib" -I"/home/ne23kaj2/programme/ext/newrepo/iccfpga_wallet/iccfpga-eclipse/submodule/ArduinoJson/src" -I"/home/ne23kaj2/programme/ext/newrepo/iccfpga_wallet/iccfpga-eclipse/submodule/iccfpga-iota-lib/src" -I"/home/ne23kaj2/programme/ext/newrepo/iccfpga_wallet/iccfpga-eclipse/src/sha512" -I"/home/ne23kaj2/programme/ext/newrepo/iccfpga_wallet/iccfpga-eclipse/submodule/trezor-crypto" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


